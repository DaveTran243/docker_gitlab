# Docker Lab

This lab helps you to practice a complete Docker build CI/CD pipeline with zero cost. By finishing this lab, you will have a full picture of best practices of how a Docker image should be built, tested, and publish in general, and in Gitlab CI in specific. You will also be practicing bash scripting as well to handle all the pipeline handling.

## Prerequisites

### Security and Compliance

`IMPORTANT`: In order to avoid any incident, please read [Security and Compliance](https://gitlab.com/devops-1012/documentation#security-and-compliance) documentation carefully before doing this lab.

### Contribution Guideline

`IMPORTANT`: For the consistent training and verification process, please read and follow the [Contribution Guideline](https://gitlab.com/devops-1012/documentation#contribution-guideline).

## Self Research

For the best lab completion, please research the following topics before handling on the lab:

### Docker

Please research and understand:

- Docker concept and design.
- Docker features.
- Docker registry (Eg: Docker Hub).
- Dockerfile.
- Google Container Structure Test.
- Dockerfile linting (Hadolint).
- Docker in Docker.

Some common questions that might help to verify your self-research result:

- What is Docker and what it's for?
- What are basic steps to build a Docker image?
- How to compress a Docker image to file and re-load it again?
- What is the different between Docker CLI and Docker engine?
- What is the different between Docker version for Linux and Windows?
- What is Docker Image and Container?
- What is Dockerfile and what it's for?
- What is `LABEL` in Dockerfile and what it's for?
- What is Docker registry?
- What is Docker in Docker (dind)?

### Gitlab CI

Please research and understand:

- Gitlab CI concept and design.
- Gitlab CI features.
- Gitlab Runner.

Some common questions that might help to verify your self-research result:

- What are `stage`, `job`, `image`, `script`, `before_script`, `tags` in Gitlab CI?
- What is Gitlab Runner and what it's for?
- How many executors available in Gitlab Runner and what's the difference?
- What does that mean when a CI variable gets masked?
- What does that mean when a CI variable gets protected?
- What does `environments` mean when setting up CI variable?
- What is `artifact` in Gitlab CI and how to copy a file from a job to a different job?
- How to setup a Docker in Docker (dind) job in Gitlab CI?
- What is `Merge Request` and what it's for?

### Linux Bash

Please research and understand:

- Basic syntax of Bash.
- Exception handling.

Some common questions that might help to verify your self-research result:

- How to declare a function in Bash script?
- How to terminate a job when error occurs?
- How to read a file?

### Semantic Versioning

Please research and understand:

- Convention of semantic versioning. Reference: https://semver.org/

Some common questions that might help to verify your self-research result:

- When to increase `major`, `minor` and `patch` version?

## Lab Requirement

For the consistent training and verification process, please read and follow the [Contribution Guideline](https://gitlab.com/devops-1012/documentation#contribution-guideline).

### Repository Structure

```
azure/
    az-cli/
        Dockerfile
baseline/
    centos/
        Dockerfile
.gitlab-ci.yml
README.md
```

### Docker Images

All Dockerfile has to follow the following schema:

```Dockerfile
FROM <base-image>

LABEL name="<image-name>" \
    version="<major>.<minor>.<build>"
```

Example:

```Dockerfile
FROM centos

LABEL name="baseline/centos" \
    version="0.1.0"

CMD ["/bin/bash"]
```

#### baseline/centos docker image

- Base image: centos
- Install the following tools:
  - `git` version 2.36.1
  - `jq` version 1.6
  - `bash-completion`
- Test:
  - Label matches pattern: `[0-9].[0-9].[0-9]`
  - `git` is installed successfully with the given version.
  - `jq` is installed successfully with the given version.
  - `bash-completion` is installed successfully.

#### azure/az-cli

- Base image: baseline/centos
- Install the following tools:
  - `az-cli` version 2.37.0
- Test:
  - Label matches pattern: `[0-9].[0-9].[0-9]`
  - `az-cli` is installed successfully with the given version.

### CI Pipeline

Implement `.gitlab-ci.yml` with the following requirements:

| Stage   | Job                      | Description                                                                                                                                 |
| ------- | ------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------- |
| prereq  | find-dockerfile          | Find all Dockerfiles within the repository. Do not hardcode the path to Dockerfile in the job. The finding has to be dynamic.               |
| lint    | hadolint                 | Use `hadolint` to lint all the found Dockerfiles available in the repository.                                                               |
| build   | dind-build               | Use Gitlab's dind to build docker images from Dockerfiles. Note that the image `name` and `version` must be read from Dockerfile's `LABEL`. |
| test    | container-structure-test | Use `Google Container Structure Test` tool to perform integration test against the built images.                                            |
| publish | specific-tag             | Publish built image to Docker Hub with the specific tag defined in the Dockerfile's `LABEL`.                                                |
|         | latest-tag               | It should be a `manual` job. Publish built image to Docker Hub with the `latest` tag.                                                       |
| git-tag | git-tag                  | Only if `latest-tag` job runs, create a new git tag by the following naming convention: `<branch-name>/<image-image>:<image-version>`.      |
